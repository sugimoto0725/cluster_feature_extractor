//#include "ros/ros.h"
#include "cluster_feature_extractor_node.h"


FeatureExtractor::FeatureExtractor() {
  base_name_="base_link";
  target_name_="map";

  is_lrf_data_=false;
  is_cluster_data_=false;

  cluster_posi_world_.resize(4);
  cluster_velo_world_.resize(4);

  cluster_feature_pub_ = nh_.advertise<kbys_msgs::PoseTwistWithCovarianceArrayStamped>("/cluster_feature",10);
  laser_data_ = nh_.subscribe("/hsrb/base_scan",10, &FeatureExtractor::LaserInfoCallback,this);
  cluster_data_ = nh_.subscribe("/human_cluster", 10, &FeatureExtractor::ClusterFeatureCallback,this);
}
FeatureExtractor::~FeatureExtractor(){}
FeatureExtractor::FeatureExtractor(const FeatureExtractor& obj){}

void FeatureExtractor::getTransformMatrix() {
  ros::Time now=ros::Time(0);
  static tf::TransformListener tf_listener;
  tf::StampedTransform transform;
  try {
    tf_listener.waitForTransform(base_name_, target_name_, now, ros::Duration(1.0));
    tf_listener.lookupTransform(base_name_, target_name_, now, transform);
  }
  catch (tf::TransformException ex) {
    ROS_ERROR("%s", ex.what());
    ros::Duration(1.0).sleep();
  }
  tf::Matrix3x3 rot_wtor(transform.getRotation());
  tf::Vector3 vec_wtor = transform.getOrigin();
  {
    tf::matrixTFToEigen(rot_wtor, mat_tmp_);
    tf_matrix_.block(0, 0, 3, 3) = mat_tmp_;
  }
  {
    tf::vectorTFToEigen(vec_wtor, vec_tmp_);
    tf_matrix_.block(0, 3, 3, 1) = vec_tmp_;
  }
}

bool FeatureExtractor::isFollowCluster(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& cluster) {
//  std::vector<double> cluster_potential;
//  std::vector<double> temp_cluster_potential;
//  double rad_increment, variance, A=10 ,alfa=0.06;
//  for(int i=0; i<cluster->states.size(); i++){
//    {
//      cluster_posi_world_[0]=cluster->states[i].position.x;
//      cluster_posi_world_[1]=cluster->states[i].position.y;
//      cluster_posi_world_[2]=cluster->states[i].position.z;
//      cluster_posi_world_[3]=1;
//      cluster_posi_robot_ = tf_matrix_*cluster_posi_world_;
//      cluster_posi_poor_[0]=sqrt(pow(cluster_posi_robot_[0],2)+pow(cluster_posi_robot_[1],2));
//      cluster_posi_poor_[1]=atan2(cluster_posi_robot_[1],cluster_posi_robot_[0]);
//    }
//    {
//      cluster_velo_world_[0]=cluster->states[i].twist.linear.x;
//      cluster_velo_world_[1]=cluster->states[i].twist.linear.y;
//      cluster_velo_world_[2]=cluster->states[i].twist.linear.z;
//      cluster_velo_world_[3]=1;
//      cluster_velo_robot_ = tf_matrix_ * cluster_velo_world_;
//    }
//
//    if(cluster->states[i].covariance[0] > cluster->states[i].covariance[10]){
//      variance=cluster->states[i].covariance[0];
//    }
//    else{
//      variance=cluster->states[i].covariance[10];
//    }
//    int j=0;
//    for(double angle=laser_angle_min_; angle<laser_angle_max_; angle+=laser_angle_increment_) {
//      temp_cluster_potential.push_back(A* cluster_velo_robot_[0] * exp(-0.5 * (pow(angle - cluster_posi_poor_[1], 2))
//                                                                  /(alfa*(0.2+variance))) /cluster_posi_poor_[0]);
//      j++;
//    }
//    if(cluster_potential.size()!=temp_cluster_potential.size()){
//      cluster_potential.resize(temp_cluster_potential.size());
//    }
//    std::transform(temp_cluster_potential.begin(), temp_cluster_potential.end(), cluster_potential.begin(),
//                   cluster_potential.begin(),std::plus<double>());
//    temp_cluster_potential.clear();
//  }
//  for(int k=0; k<cluster_potential.size();k++){
//    if(cluster_potential[k]<0){
//      cluster_potential.clear();
//      return true;
//    }
//  }
//  cluster_potential.clear();
//  return false;
}

void FeatureExtractor::transformClusterCoordinate(const kbys_msgs::PoseTwistWithCovarianceArrayStamped& cluster){
  for(int i=0; i<cluster.states.size(); i++) {
    {
      cluster_posi_world_[0] = cluster.states[i].position.x;
      cluster_posi_world_[1] = cluster.states[i].position.y;
      cluster_posi_world_[2] = cluster.states[i].position.z;
      cluster_posi_world_[3] = 1;
      cluster_posi_robot_ = tf_matrix_ * cluster_posi_world_;
    }
    {
      cluster_velo_world_[0] = cluster.states[i].twist.linear.x;
      cluster_velo_world_[1] = cluster.states[i].twist.linear.y;
      cluster_velo_world_[2] = cluster.states[i].twist.linear.z;
      cluster_velo_world_[3] = 1;
      cluster_velo_robot_ = tf_matrix_ * cluster_velo_world_;
    }
    cluster_in_robot_axis_.states.resize(cluster.states.size());
    cluster_in_robot_axis_.states[i].position.x=cluster_posi_robot_[0];
    cluster_in_robot_axis_.states[i].position.y=cluster_posi_robot_[1];
    cluster_in_robot_axis_.states[i].position.z=cluster_posi_robot_[3];
    cluster_in_robot_axis_.states[i].twist.linear.x=cluster_velo_robot_[0];
    cluster_in_robot_axis_.states[i].twist.linear.y=cluster_velo_robot_[1];
    cluster_in_robot_axis_.states[i].twist.linear.z=cluster_velo_robot_[2];
    cluster_in_robot_axis_.states[i].covariance=cluster.states[i].covariance;
  }
  return;
}

void FeatureExtractor::LaserInfoCallback(const sensor_msgs::LaserScan::ConstPtr& laser){
  laser_angle_max_=laser->angle_max;
  laser_angle_min_=laser->angle_min;
  laser_angle_increment_=laser->angle_increment;
  is_lrf_data_=true;
  return;
}

void FeatureExtractor::ClusterFeatureCallback(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& cluster)
{
  cluster_in_world_axis_=*cluster;
  is_cluster_data_=true;
  return;
}

void clusterfda(const kbys_msgs::PoseTwistWithCovarianceArrayStamped& cluster){
  return;
}

void FeatureExtractor::getClusterFeature() {
  getTransformMatrix();
  kbys_msgs::PoseTwistWithCovarianceArrayStamped cluster;
  transformClusterCoordinate(cluster_in_world_axis_);
  cluster_in_robot_axis_.header=cluster_in_world_axis_.header;

  is_lrf_data_=false;
  is_cluster_data_=false;
}

void FeatureExtractor::publishTopics() {
  cluster_feature_pub_.publish(cluster_in_robot_axis_);
  ROS_INFO("publish cluster feature!");
  return;
}

void FeatureExtractor::periodicActivator() {
  ros::Rate rate(10);
  while(nh_.ok()){
    ros::spinOnce();
    if(is_lrf_data_&&is_cluster_data_){
      getClusterFeature();
      publishTopics();
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "cluster_feature_extractor");
  try{
    FeatureExtractor extractor;
    extractor.periodicActivator();
  }catch (const std::exception& msg){
    ROS_FATAL(msg.what());
    return EXIT_FAILURE;
  }
  return 0;
}