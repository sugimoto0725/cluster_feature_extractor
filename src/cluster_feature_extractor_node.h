#ifndef PROJECT_CLUSTER_FEATURE_EXTRACTOR_NODE_H
#define PROJECT_CLUSTER_FEATURE_EXTRACTOR_NODE_H
#include "ros/ros.h"
#include "kbys_msgs/PoseTwistWithCovarianceArrayStamped.h"
#include "sensor_msgs/LaserScan.h"
#include "nav_msgs/Odometry.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

class FeatureExtractor {
public:
    //ros
    ros::NodeHandle nh_;
    ros::Publisher cluster_feature_pub_;
    ros::Subscriber cluster_data_;
    ros::Subscriber laser_data_;

    //ros_msg members
    kbys_msgs::PoseTwistWithCovarianceArrayStamped cluster_in_world_axis_;
    kbys_msgs::PoseTwistWithCovarianceArrayStamped cluster_in_robot_axis_;

//    tf members
    Eigen::Matrix4d tf_matrix_; // transposed matrix world to robot coordinate
    std::string base_name_;
    std::string target_name_;
    Eigen::Matrix3d mat_tmp_;
    Eigen::Vector3d vec_tmp_;
    Eigen::Vector4d cluster_posi_world_;
    Eigen::Vector4d cluster_posi_robot_;
    Eigen::Vector2d cluster_posi_poor_;
    Eigen::Vector4d cluster_velo_world_;
    Eigen::Vector4d cluster_velo_robot_;

    //laser info members
    double laser_angle_max_;
    double laser_angle_min_;
    double laser_angle_increment_;

    //checker for reception of topics
    bool is_lrf_data_;
    bool is_cluster_data_;

    //member functions
    FeatureExtractor();
    FeatureExtractor(const FeatureExtractor& obj);
    ~FeatureExtractor();
    bool isFollowCluster(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& cluster);
    void getTransformMatrix();
    void transformClusterCoordinate(const kbys_msgs::PoseTwistWithCovarianceArrayStamped& cluster);
    void LaserInfoCallback(const sensor_msgs::LaserScan::ConstPtr& laser);
    void ClusterFeatureCallback(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& cluster);
    void getClusterFeature();
    void periodicActivator();
    void publishTopics();
};
#endif //PROJECT_CLUSTER_FEATURE_EXTRACTOR_NODE_H
